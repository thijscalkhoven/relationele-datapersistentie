/* Bekijken van meest populaire producten */
SELECT `id`, `name`
FROM `Products`
INNER JOIN (SELECT SUM(`quantity`) AS totalAmountOrdered, `productId`
FROM `OrdersProducts`
GROUP BY `productId`
ORDER BY `totalAmountOrdered` DESC
LIMIT 8) AS MostSoldProducts
ON `Products`.`id` = `MostSoldProducts`.`productId`;

/* Account aanmaken */
INSERT INTO `Users`(`email`, `password`, `profilePicture`, `firstName`, `lastName`, `birthDate`, `address`, `city`, `region`, `country`, `postalCode`, `validatedEmail`) VALUES ('t.calkhoven@live.nl','cf23df2207d99a74fbe169e3eba035e633b65d94','pf/123','Thijs','Calkhoven','1998-01-30','Slotlaan 8','Lage Vuursche','NL-UT','NL','3749 AA',1);

/* Inloggen */
SELECT `email`, `password` FROM `Users` WHERE `email` = 't.calkhoven@live.nl' AND `password` = 'cf23df2207d99a74fbe169e3eba035e633b65d94';

/* Wachtwoord vergeten */
INSERT INTO `PasswordResets`(`userId`, `token`) VALUES (101,'94e6d485-346a-4702-b9a8-1006a3b13223');

/* Remember me */
UPDATE `Users` SET `rememberToken`='fb2e77d.47a0479900504cb3ab4a1f626d174d2d' WHERE `id`=101;

/* E-mailadres validatie */
INSERT INTO `EmailValidations`(`userId`, `token`) VALUES (101,'94e6d485-346a-4702-b9a8-1006a3b13223');

/* Overzicht van producten */
SELECT * FROM `Products`;

/* Overzicht van seeds */
SELECT *
FROM `Products`
INNER JOIN `Seeds` ON `Products`.`id` = `Seeds`.`productId`
WHERE `Products`.`type`='seed';

/* Overzicht van smartgardens */
SELECT *
FROM `Products`
INNER JOIN `SmartGardens` ON `Products`.`id` = `SmartGardens`.`productId`
WHERE `Products`.`type`='smartgarden';

/* Winkelmand van user 9 */
SELECT `Products`.`name`, `OrdersProducts`.`quantity`, `Products`.`description`, `Products`.`price`, `Products`.`image` 
FROM `Orders`
INNER JOIN `Users` ON `Users`.`id` = `Orders`.`userId`
INNER JOIN `OrdersProducts` ON `OrdersProducts`.`orderId` = `Orders`.`id`
INNER JOIN `Products` ON `OrdersProducts`.`productId` = `Products`.`id`
WHERE `Orders`.`paid`=0 AND `Orders`.`userId`=9;

/* Kopen van producten voor user 9 koopt product 20 */
INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=9),20,2);

INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=9),18,1);

INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=9),6,16);

INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=9),2,12);

INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=8),20,2);

INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=8),18,1);

INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=8),6,16);

INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=8),2,12);

/* Product suggesties voor product 20 */
SELECT `Products`.`name`, `Products`.`description`, `Products`.`price`, `Products`.`image` 
FROM  (SELECT `productId`
FROM `OrdersProducts`
WHERE `orderId` IN((SELECT `orderId` FROM `OrdersProducts` WHERE `productId` = 20))
GROUP BY `productId`
HAVING `productId` <> 20
ORDER BY SUM(`quantity`) DESC
LIMIT 3) AS PopularProducts
INNER JOIN `Products` ON `Products`.`id` = `PopularProducts`.`productId`;

/* Product Rating voor user 9 over product 20*/
INSERT INTO `ProductRatings`(`userId`, `productId`, `userRating`, `userFeedback`) 
VALUES (9,20,5,'Best Product Ever!');

/* Weergeven data SmartGarden user 9 */
SELECT `SmartGardenData`.`lightIntensity`, `SmartGardenData`.`oxygenValue`, `SmartGardenData`.`humidityValue`, `SmartGardenData`.`nitrogenValue`, `SmartGardenData`.`createdAt`
FROM `SmartGardenData`
INNER JOIN `OwnedSmartGardens` ON `OwnedSmartGardens`.`id` = `SmartGardenData`.`smartGardenId`
INNER JOIN `Users` ON `Users`.`id` = `OwnedSmartGardens`.`userId`
WHERE `Users`.`id`=9;

/* Gebruikers gegevens */
SELECT * FROM `Users`;

/* Abonneren op dienst */
UPDATE `Users` SET `subscriptionEndDate`='2020-01-30' WHERE `id`=9;