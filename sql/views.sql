/* Bekijken van meest populaire producten */
CREATE VIEW PopularProducts AS 
    SELECT `id`, `name` 
    FROM `Products` 
    INNER JOIN (SELECT SUM(`quantity`) AS totalAmountOrdered, `productId` 
        FROM `OrdersProducts` 
        GROUP BY `productId` 
        ORDER BY `totalAmountOrdered` DESC) AS MostSoldProducts 
    ON `Products`.`id` = `MostSoldProducts`.`productId`;

SELECT * FROM `PopularProducts`;