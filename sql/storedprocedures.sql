/* Bekijken van meest populaire producten */
DELIMITER //
CREATE PROCEDURE GetPopularProductsByAmount(IN amount INT)
  BEGIN
  SELECT `id`, `name`
    FROM `Products`
    INNER JOIN (SELECT SUM(`quantity`) AS totalAmountOrdered, `productId`
    FROM `OrdersProducts`
    GROUP BY `productId`
    ORDER BY `totalAmountOrdered` DESC
    LIMIT amount) AS MostSoldProducts
    ON `Products`.`id` = `MostSoldProducts`.`productId`;
  END //
DELIMITER ;

CALL GetPopularProductsByAmount(20);

/* Product suggesties voor product x tot suggesties y */
DELIMITER //
CREATE PROCEDURE GetPopularAssociatedProductsByAmount(
 IN idOfProduct INT,
 IN amount INT)
    BEGIN
        SELECT `Products`.`name`, `Products`.`description`, `Products`.`price`, `Products`.`image` 
        FROM  (SELECT `productId`
        FROM `OrdersProducts`
        WHERE `orderId` IN((SELECT `orderId` FROM `OrdersProducts` WHERE `productId` = idOfProduct))
        GROUP BY `productId`
        HAVING `productId` <> idOfProduct
        ORDER BY SUM(`quantity`) DESC
        LIMIT amount) AS PopularProducts
        INNER JOIN `Products` ON `Products`.`id` = `PopularProducts`.`productId`;
    END //
DELIMITER ;

/* Werkt niet met empty response in phpmyadmin! */
CALL GetPopularAssociatedProductsByAmount(20, 5);