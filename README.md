# NotEppol Database

Dit project demonstreert hoe de NotEppol database zal werken. Dit gebeurd door eerst de database aan te maken en vervolgens via een webserver toegang te bieden tot de queries die de functionaliteiten mogelijk maken die beschreven staan in de analyse van eisen en wensen.

## Getting Started

Clone dit project in een lokale folder via GIT.

### Prerequisites

[Instaleer Docker CE via deze link](https://docs.docker.com/install/)

### Installing

Nadat docker is geïnstalleerd en de repository is gekloond, open de folder in een terminal.

Type hier de volgende statement

```
docker-compose -f "docker-compose.yml" up
```

Wacht totdat alles geladen is en de containers klaar staan.

Test hierna of de containers inderdaad aangemaakt zijn door naar de volgende twee links te gaan:

- [http://localhost:1337 voor de website](http://localhost:1337)
- [http://localhost:8080 voor phpmyadmin](http://localhost:8080)

Als dit goed gegaan is, kan er worden genavigeerd naar phpmyadmin.
Hier log je in met username: root en wachtwoord: 1337.
Importeer hier de 'database-setup.sql' en check of de database noteppol is aangemaakt.

Navigeer nu naar de website and you're all set to go!

## Authors

* **Thijs Calkhoven** 
