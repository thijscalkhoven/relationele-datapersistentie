$(document).ready(function () {
    function createTable(id, array) {
        let element = $(`#${id}`);
        element.html('');
        console.log(array);

        let table = '<table class="table"><thead><tr>';
        array[0].forEach(item => {
            table += `<th scope="col">${item}</th>`;
        });
        table += '</tr></thead><tbody>';

        for (let i = 1; i < array.length; i++) {
            let row = array[i];
            table += '<tr>';
            array[0].forEach(item => {
                table += `<td>${row[item]}</td>`;
            });
            table += '</tr>';
        }

        table += '</tbody></table>';
        console.log(table);
        element.html(table);
    }

    $('#get1').click(function () {
        $.ajax({
            url: './api.php?function=popularproducts',
            success: function (result) {
                createTable('res1', JSON.parse(result));
            }
        });
    });

    $('#get2').submit(function (e) {
        e.preventDefault();

        $.ajax({
            url: './api.php',
            type: 'post',
            data: $(this).serialize(),
            success: function (result) {
                createTable('res2', JSON.parse(result));
            }
        });
    });

    $('#get3').click(function () {
        $.ajax({
            url: './api.php?function=seeds',
            success: function (result) {
                createTable('res3', JSON.parse(result));
            }
        });
    });

    $('#get4').click(function () {
        $.ajax({
            url: './api.php?function=aankopen',
            success: function (result) {
                if (result === 'OK') {
                    $('#res4').html('<p>Aankopen zijn gemaakt!</p>');
                }
            }
        });
    });

    $('#get5').click(function () {
        $.ajax({
            url: './api.php?function=winkelmand',
            success: function (result) {
                createTable('res5', JSON.parse(result));
            }
        });
    });
});