<?php
$dsn = "mysql:host=database;port=3306;dbname=noteppol;charset=utf8mb4";

try {
    $conn = new PDO($dsn, "application", "securepassword!");
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

if($_GET['function'] === 'popularproducts') {
    $query = "SELECT id, name FROM Products ";
    $query .= "INNER JOIN (SELECT SUM(quantity) AS totalAmountOrdered, productId ";
    $query .= "FROM OrdersProducts GROUP BY productId ORDER BY totalAmountOrdered DESC LIMIT 8) AS MostSoldProducts ";
    $query .= "ON Products.id = MostSoldProducts.productId";

    $response = array(array('id', 'name'));
    $stmt = $conn->query($query);
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        array_push($response, $row);
    }
    
    echo json_encode($response);
}

if(isset($_POST['email']) && isset($_POST['password'])) {
    $query = "INSERT INTO `Users`(`email`, `password`, `profilePicture`, `firstName`, `lastName`, `birthDate`, `address`, `city`, `region`, `country`, `postalCode`, `validatedEmail`) VALUES (:email,:password,'pf/123','Thijs','Calkhoven','1998-01-30','Slotlaan 8','Lage Vuursche','NL-UT','NL','3749 AA',1)";
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':email' => $_POST['email'], ':password' => $password));

    $query = "SELECT `email`, `password` FROM `Users` WHERE id=:id";
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':id' => $conn->lastInsertId()));

    $response = array(array('email', 'password'));
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        array_push($response, $row);
    }

    echo json_encode($response);
}

if($_GET['function'] === 'seeds') {
    $query = "SELECT Products.name, Products.description, Products.price, Products.stock, Seeds.amountPackaged, Seeds.growTime FROM `Products` INNER JOIN `Seeds` ON `Products`.`id` = `Seeds`.`productId` WHERE `Products`.`type`='seed';";

    $response = array(array('name', 'description', 'price', 'stock', 'amountPackaged', 'growTime'));
    $stmt = $conn->query($query);
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        array_push($response, $row);
    }
    
    echo json_encode($response);
}

if($_GET['function'] === 'aankopen') {
    $query = "INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
    VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=9),20,2);
    
    INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
    VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=9),18,1);
    
    INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
    VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=9),6,16);
    
    INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
    VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=9),2,12);
    
    INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
    VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=8),20,2);
    
    INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
    VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=8),18,1);
    
    INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
    VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=8),6,16);
    
    INSERT INTO `OrdersProducts`(`orderId`, `productId`, `quantity`) 
    VALUES ((SELECT `id` FROM `Orders` WHERE `paid`=0 AND `userId`=8),2,12);";

    $stmt = $conn->prepare($query);
    $stmt->execute();

    echo "OK";
}

if($_GET['function'] === 'winkelmand') {
    $query = "SELECT `Products`.`name`, `OrdersProducts`.`quantity`, `Products`.`description`, `Products`.`price`, `Products`.`image` 
    FROM `Orders`
    INNER JOIN `Users` ON `Users`.`id` = `Orders`.`userId`
    INNER JOIN `OrdersProducts` ON `OrdersProducts`.`orderId` = `Orders`.`id`
    INNER JOIN `Products` ON `OrdersProducts`.`productId` = `Products`.`id`
    WHERE `Orders`.`paid`=0 AND `Orders`.`userId`=9;";

    $response = array(array('name', 'quantity', 'description', 'price', 'image'));
    $stmt = $conn->query($query);
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        array_push($response, $row);
    }
    
    echo json_encode($response);
}